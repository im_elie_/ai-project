<h1 align="center">
  <br>
  AI project - One pizza is all you need
  <br>
</h1>

<h4 align="center">Une solution au Google Hashcode 2022 : One pizza.</h4>

<p align="center">
  <a href="#contexte">Contexte</a> •
  <a href="#auteurs">Auteurs</a> •
  <a href="#besoins-requirements">Besoins (Requirements)</a> •
  <a href="#utilisation">Utilisation</a> •
  <a href="#license">License</a>
</p>

## Auteurs

👤 **Théo GOUREAU, Cyrielle LACRAMPE--DITER, Damien SIMON**

## Contexte
Ce projet d'intelligence artificielle s'inscrit dans la deuxième année de formation à [TELECOM Nancy](https://telecomnancy.univ-lorraine.fr). Le sujet est disponible [ici](./IA_Projet2223-pizza.pdf) et s'inspire d'un sujet de Google Hashcode.

## Besoins (Requirements)
Vous aurez besoin d'une machine (ordinateur, serveur, ...) ayant un système d'exploitation [Linux](https://www.linux.org/).

Pour cloner et utiliser ce dépôt, vous aurez besoin de [Git](https://git-scm.com), de [make](https://www.gnu.org/software/make/) et de [gcc](https://gcc.gnu.org/). 

## Utilisation
Depuis votre terminal :

```bash
# Cloner le dépôt
$ git clone https://gitlab.com/im_elie_/ai-project.git

# Aller dans le dépôt
$ cd ai-project

# Compiler
$ make

# Lancer
$ ./main --help
```

Les trois algorithmes proposés sont :
1. gen - Algorihtme génétique
2. exp - Recherche explicite
3. recuit - Recuit simulé

L'option `--fep` permet de multithreader une partie de l'algorithme génétique.

L'option `--graph` permet d'obtenir un fichier csv qui contient une colonne d'avancement (itération ou température) et une colonne de score. On peut ainsi créer un graph de l'évolution du score.

L'entrée et la sortie utilisent un set basé sur une hashmap. `test_io` permet de vérifier le bon fonctionnement de son implémentation et de voir le nombre maximal d'éléments pour un même hash.

## License

[MIT](LICENSE.md)

---

<p align="center">
  Web <a href="https://www.goureau.eu/">www.goureau.eu</a> •
  GitLab <a href="https://gitlab.com/im_elie_">@im_elie_</a> •
  Mail <a href="mailto:git.ai-project@theo.goureau.eu">git.ai-project@theo.goureau.eu</a>
</p>

<pre style="margin-left: 42%; color: yellow;">
   __
  // \
  \\_/ //
 -(||)(')
  '''
</pre>

